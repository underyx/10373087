### Keybase proof

I hereby claim:

  * I am underyx on github.
  * I am underyx (https://keybase.io/underyx) on keybase.
  * I have a public key whose fingerprint is B07D 23CA 602F 5C5A D147  C2DD E45A 2B0E B35D 2590

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "fingerprint": "b07d23ca602f5c5ad147c2dde45a2b0eb35d2590",
            "host": "keybase.io",
            "key_id": "E45A2B0EB35D2590",
            "uid": "f5dfc74247e15016e9782d9d44fd4400",
            "username": "underyx"
        },
        "service": {
            "name": "github",
            "username": "underyx"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1397203981,
    "expire_in": 157680000,
    "prev": "900b09b2026d5fae890b867e3e6b1a2c8bdfa8ee6910d51074de890a4f42eaf0",
    "seqno": 10,
    "tag": "signature"
}
```

with the PGP key whose fingerprint is
[B07D 23CA 602F 5C5A D147  C2DD E45A 2B0E B35D 2590](https://keybase.io/underyx)
(captured above as `body.key.fingerprint`), yielding the PGP signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v1

owGbwMvMwMQYcWC3yz+96ATG0wdeJjEEuy/hrVZKyk+pVLKqVspOBVNpmXnpqUUF
RZl5JUpWSkkG5ilGxsmJZgZGaabJpokphibmyUYpKakmpolGSQapScamKUamlgZK
OkoZ+cUgHUBjkhKLU/Uy84FiQE58ZgpQ1NXE1NHIycDVydjUBaq+FCyRZpqSlmxu
YmRinmpoamBolmppbmGUYpliYpIGxAZghcWpRXmJualA1aV5KalFlRVKtTpKQMGy
zORUkJOhkumZJRmlSTg1lFQWgETKU5PioXrjkzLzUoDeBWopSy0qzszPU7IyBKpM
LskEaTY0tjQ3MjC2tDDUUUqtKMgsSo3PBKkwNTezMAACHaWCotQyoJGWBgZJBpZJ
RgZGZimmaYmpFpYGSRZm5qnGqWZJholGyRZJKWmJFqmpZpaGBimmhgbmJikgNYkm
aSZGqYlpID8Wpxbm5QPNBhpakpgONLM4Mz0vsaS0KFWptpNRhoWBkYmBjZUJFGMM
XJwCsHiMv8rBMM24hvfuZQWFgsD9FQv8zh0xvj9nz8l/XuwmE5b9eT99i+O+PSGf
9leZnvDiqxI6WJzzqsF1qeyvuJLzjjI84TJfP71Sz9X1nj9vbsi9xKTv4r/4/yaw
XbqoUNO99eckjTapvV32chdLlhwpP8/9V8owRtgnN9BA4dye8IsJrbuj8ua8WmHX
3VT2fscE2btL5h6cXsP+6YeY7oma09w8AolZqxzCpgb8lnzwcYqW6X39dQfFnZKv
61y6dZjn2I0QLhu5YwsObpixeK7i8kXWU34rhD44n1j4PFzne+WUmxM/5v+d+va/
x7Xu1+zlIfLtUmkTEtIyDlRsPFkT5MXG6ht8tjmRZUdaw5+320uZpwEA
=LGvu
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/underyx

### From the command line:

Consider the [keybase command line program](https://keybase.io/__/command_line/keybase).

```bash
# look me up
keybase id underyx

# encrypt a message to me
keybase encrypt underyx -m 'a secret message...'

# ...and more...
```